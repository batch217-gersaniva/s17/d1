// console.log("Hello World");

// [SECTION]  Functions
// Functions in javascript are  lines / blocks of codes

/*
Syntax:
	function functionName() {
		code block (statement)
	}
*/

// function keyword - used to define a js functions
// functionName - we set name so that we can use it for later
// function block ({}) - this is where code to be executed


function printName(){
	console.log("My name is John");
}

printName(); //Invocation - calling a function that need to be executed

function declaredFunction() {
	console.log("Hello World from declaredFunction()");
}

declaredFunction();


// Function Expression
// A function can also be stored in a variable. This is a functin expression.

let variableFunction = function(){
	console.log("Hello Again!");
}

variableFunction();

// console.log(typeof variableFunction);

let funcExpression =function funcName(){
	console.log("Hello from the other side")
}

funcExpression();

funcExpression = function(){
	console.log("Updated funcExpression")
}

funcExpression();

// re-assignning declaredFunction() value

declaredFunction = function(){
	console.log("Updated declaredFunction");
}

declaredFunction();

// Re-assigning function declared with const

const constantFunc	= function(){
	console.log("initalized with const!");
}

constantFunc();

// Re

// constantFunc = function(){
// 	console.log("Can we re-assign it?")
// }

// constantFunc();

// [SECTION] Function Scoping

// Scope is the accessibility (visibility) of variables.

// Javascript Variables has 3 types:
// 	1. local/block scope
	// 2. global scope
	// 3 function scope



{
	let localVar = "Armando Perez";
	console.log(localVar);
}

// console.log(localVar);   -----> inaccessible

let globalScope = "Mr. Worldwide";

	console.log(globalScope);


// Function Scoping

function showNames(){
	// Function 
	const functionConst ="John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// console.log(functionConst);  
// console.log(functionLet);	
// ----> cannot be used because these variables are located inside the scope of a function

// Nested Function
// You can create another function inside a function. This is called a bested function. This nested function, being inside 

function myNewFunction(){
	let name = "Jane";   //inhereted data

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}

// myNewFunction();
// nestedFunction(); ---> will cause error due to outside parent function.

// Function and Global Scoped Variable

// Global scoped variable

let globalName = "Alendro";

function myNewFunction2(){
	let nameInside ="Renz";	

	// Variable 

	console.log(globalName);

}

myNewFunction2();
// console.log(nameInside);

// [SECTION] Using Alert()
// alert() allows

alert("Hello World!"); //This will be executed immediately

function showNames(){
	alert("Hello user");
}

showNames();

console.log("I will only log in the console when alert is dismissed.");


// Notes on the use of alert():


// [SECTION] Using Prmopt()
// 

// let samplePrompt = prompt("Enter your name.");

// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

// prompt() can be used to gather user input 
// prompt it can be run immediately

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name. ");
	let lastName = prompt("Enter your last name. ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

// [SECTION] Function Name Convention


function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourse();

// Avoid generic names

function getName() {     //used getName instead of "get"
	let name = "Jamie";
	console.log(name)
}

getName();

// 

function getMod(){
	console.log(25%5);
}

getMod();

// Name your function in small caps. Follow camelCase whuen naming variables and functions.

// camelCase ---> myNameIsAprilMarie
// snake_case
// kebab-case

function displayCarInfo(){
	console.log("Brand: Toyota")
	console.log("Type: Sedan")
	console.log("Price: 1, 500, 000")
}

displayCarInfo();
